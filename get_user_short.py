from doctest import testmod
import logging


def get_user_short(usr=john.doe, chars_firstname=2, chars_lastname=3):
    """
    Creates shortened version of user name.
    Expects name.surname as an input
    Tries to make the short as long as chars_firstname + chars_lastname

    >>> get_user_short(usr='john.doe', chars_firstname=2, chars_lastname=3)
    'jodoe'
    >>> get_user_short(usr='john', chars_firstname=2, chars_lastname=3)
    'john'
    >>> get_user_short(usr='joh.', chars_firstname=2, chars_lastname=3)
    'joh'
    >>> get_user_short(usr='j.doe', chars_firstname=2, chars_lastname=3)
    'jdoe'
    >>> get_user_short(usr='karel.hynek.macha', chars_firstname=2, chars_lastname=3)
    'kamac'
    >>> get_user_short(usr='john.do', chars_firstname=2, chars_lastname=3)
    'johdo'
    >>> get_user_short(usr=None, chars_firstname=2, chars_lastname=3)
    ''
    """

    if usr is None:
        log = logging.getLogger("User name can't be shortened.")
        return ''

    chars_total = chars_firstname + chars_lastname
    usr_split = usr.split('.')
    _names = len(usr_split)
    if _names <= 1:
        _first_name = usr_split[0]
        _last_name = ''
    elif _names == 2:
        _first_name = usr_split[0]
        _last_name = usr_split[1]
    else:
        _first_name = usr_split[0]
        _last_name = usr_split[-1]

    _f = _first_name[:chars_firstname]
    _l = _last_name[:chars_lastname]
    usr_short = _f + _l
    _prolong = chars_total - len(usr_short)
    if len(usr_short) < chars_total:
        # try to prolong by adding more chars from last name
        _l = _last_name[:chars_lastname + _prolong]
        usr_short = _f + _l
    _prolong = chars_total - len(usr_short)
    if len(usr_short) < chars_total:
        # try to prolong by adding more chars from first name
        _f = _first_name[:chars_firstname + _prolong]
        usr_short = _f + _l

    log = logging.getLogger("User {usr} is shorted to {usr_short}".format(usr=usr, usr_short=usr_short))
    return usr_short


if __name__ == '__main__':
    testmod(name='get_user_short', verbose=True)

    import os
    get_user_short(os.getusername(), 3, 2)
